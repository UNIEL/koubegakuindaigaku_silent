import { stylesTypes } from '~/components/styles'

declare module 'styled-components' {
  interface DefaultTheme extends stylesTypes {}
}

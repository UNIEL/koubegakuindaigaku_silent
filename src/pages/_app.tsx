import React from 'react'
import type { AppProps } from 'next/app'
import { AppLayout } from '~/components/shared/AppLayout'
import ThemeWrapper from '~/components/styles'
import '~/css/reset.scss'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeWrapper>
      <AppLayout>
        <Component {...pageProps} />
      </AppLayout>
    </ThemeWrapper>
  )
}

export default MyApp

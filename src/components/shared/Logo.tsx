import React from 'react'
import styled from 'styled-components'
import { CustomLink } from '~/components/utils/CustomLink'
import { getWebpImage } from '~/utils'

type ContainerProps = {
  className?: string
}

type Props = {} & ContainerProps

const Component: React.FC<Props> = (props) => {
  return (
    <h2 className={props.className}>
      <CustomLink href="/">
        <img
          src={getWebpImage('/images/common/logo.png')}
          alt="神戸学院大学 Dramati!c Campas"
        />
      </CustomLink>
    </h2>
  )
}

const StyledComponent = styled(Component)``

const Container: React.FC<ContainerProps> = ({ ...props }) => {
  return <StyledComponent {...props} />
}

export const Logo = Container

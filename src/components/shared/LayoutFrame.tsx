import React, { useEffect, useState, useMemo } from 'react'
import styled from 'styled-components'
import { useWindowSize } from 'react-window-size-hooks'
import { env } from '~/utils/environments'
import { useFrameColor } from '~/hooks/useFrameColor'

const STROKE_WIDTH = 8

type ContainerProps = {
  className?: string
}

type Props = {
  svgWidth: number
  svgHeight: number
  strokeSize: number
  strokeColor: string
} & ContainerProps

const Component: React.FC<Props> = (props) => {
  return (
    <div className={props.className}>
      <svg
        width={props.svgWidth}
        height={props.svgHeight}
        viewBox={`0, 0, ${props.svgWidth}, ${props.svgHeight}`}
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect
          x="0"
          y="0"
          width={props.svgWidth}
          height={props.svgHeight}
          strokeDasharray={props.strokeSize}
          strokeDashoffset={0}
        />
      </svg>
      {props.children}
    </div>
  )
}

const StyledComponent = styled(Component)`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  height: 100%;
  pointer-events: none;

  svg {
    fill: none;
    pointer-events: none;
  }
  rect {
    stroke: ${(props) => props.strokeColor};
    stroke-width: ${STROKE_WIDTH};
    outline-style: solid;
  }
`

const Container: React.FC<ContainerProps> = ({ ...props }) => {
  const [initialSize, setInitialSize] = useState({ width: 0, height: 0 })
  const { width, height } = useWindowSize()
  const color = useFrameColor()

  const strokeSize = useMemo(() => {
    const w = width > 0 ? width : initialSize.width
    const h = height > 0 ? height : initialSize.height
    return w * 2 + h * 2
  }, [initialSize, width, height])

  useEffect(() => {
    if (!env.isServer) {
      setInitialSize({
        width: window.innerWidth,
        height: window.innerHeight,
      })
    }
  }, [])

  const componentProps = {
    svgWidth: width > 0 ? width : initialSize.width,
    svgHeight: height > 0 ? height : initialSize.height,
    strokeColor: color,
    strokeSize,
  }

  return <StyledComponent {...props} {...componentProps} />
}

const LayoutFrame = Container

export default LayoutFrame

import React from 'react'
import dynamic from 'next/dynamic'
import styled from 'styled-components'
import { Logo } from '~/components/shared/Logo'
const LayoutFrame = dynamic(() => import('~/components/shared/LayoutFrame'), {
  ssr: false,
})

type ContainerProps = {
  className?: string
}

type Props = {} & ContainerProps

const Component: React.FC<Props> = ({ children, className }) => {
  return (
    <div className={className}>
      <LayoutFrame />
      <Logo />
      {children}
    </div>
  )
}

const StyledComponent = styled(Component)`
  height: 100%;
  padding: 4px;
`

const Container: React.FC<ContainerProps> = ({ ...props }) => {
  return <StyledComponent {...props} />
}

export const AppLayout = Container

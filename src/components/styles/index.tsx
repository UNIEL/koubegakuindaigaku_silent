import React from 'react'
import { ThemeProvider, css } from 'styled-components'
import { colors } from '~/components/styles/colors'
import { sizes } from '~/components/styles/sizes'
import { easings } from '~/components/styles/easings'
import { mixins } from '~/components/styles/mixins'

export type stylesTypes = {
  colors: typeof colors
  mixins: typeof mixins
  easings: typeof easings
  sizes: typeof sizes
}

export const styles: stylesTypes = {
  colors,
  sizes,
  easings,
  mixins,
}

// mediaquery
// sample: ${mq.small`color: #000;`}
export const mq = Object.keys(sizes.mq).reduce((acc, label) => {
  acc[label] = (literals: TemplateStringsArray, ...placeholders: any[]) =>
    css`
      @media (max-width: ${sizes.mq[label]}px) {
        ${css(literals, ...placeholders)};
      }
    `
  return acc
}, {} as Record<keyof typeof sizes.mq, (l: TemplateStringsArray, ...p: any[]) => string>)

function ThemeWrapper({ children }) {
  return <ThemeProvider theme={styles}>{children}</ThemeProvider>
}

export default ThemeWrapper

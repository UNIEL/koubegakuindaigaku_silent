import React from 'react'
import styled from 'styled-components'

type ContainerProps = {
  className?: string
}

type Props = {} & ContainerProps

const Component: React.FC<Props> = ({ className }) => {
  return <div className={className}></div>
}

const StyledComponent = styled(Component)``

const Container: React.FC<ContainerProps> = ({ ...props }) => {
  return <StyledComponent {...props} />
}

export const HomePage = Container

export type PageType =
  | 'top'
  | 'detail'
  | 'gallery'
  | 'about'
  | 'loading'
  | 'sound'

import { useMemo } from 'react'
import { colors } from '~/components/styles/colors'
import { PageType } from '~/models/page'

export const useFrameColor = () => {
  const pageType = 'top' as PageType

  const color = useMemo(() => {
    switch (pageType) {
      case 'sound':
      case 'loading':
        return colors.primaryBlue
      default:
        return colors.white100
    }
  }, [pageType])

  return color
}

const isServer = typeof window === 'undefined'
const nodeEnv = process.env.NODE_ENV

export const env = {
  isServer,
  nodeEnv,
}

import React from 'react'
import { UAParser } from 'ua-parser-js'

export const nl2br = (text: string) => {
  const regex = /(\n)/g
  return text
    .split(regex)
    .map((line, index) =>
      line.match(regex) ? React.createElement('br', { key: index }) : line
    )
}

export const getUa = () => {
  var parser = new UAParser()
  const os = parser.getOS()

  return {
    ie: parser.getBrowser().name === 'IE',
    safari:
      parser.getBrowser().name === 'Safari' ||
      parser.getBrowser().name === 'Mobile Safari',
    ios: os.name === 'iOS',
    android: os.name === 'Android',
  }
}

/**
 * 処理ストップ用関数
 * @param ms 指定秒数
 */
export const delay = (ms: number) =>
  new Promise((resolve) => setTimeout(resolve, ms))

/**
 * webp対応ブラウザ判定
 * @param path 画像パス
 */
export const getWebpImage = (path: string) => {
  const { safari, ie } = getUa()
  if (safari || ie) {
    return path
  }
  return `${path}.webp`
}
